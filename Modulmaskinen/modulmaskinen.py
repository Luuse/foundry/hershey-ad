import yaml
from flask import Flask, render_template, request
import lxml.etree as ElementTree
import json
from svgpathtools import parse_path, Line, Path, wsvg
from jinja2 import Template
import os
import glob
import random
import font_generator 


_VERSION = 17 

_YAML_FILE = 'letters-build-'+str(_VERSION)+'.yaml'

def readYaml(file_yaml):
    with open(file_yaml, 'r') as file:
        result = yaml.safe_load(file) 
    return result

def setBuilder():
    allSvg = glob.glob('../Futura-Mono-original-svg/*.svg')
    allKeys = []
    for svg in allSvg: allKeys.append(int(svg.split('/')[-1].split('.')[0]))
    allKeys.sort()

    TPL_glyph = '''
  {{key}}:
    name: '{{name}}'
    modules: '''

    _output = '''
glyphs:'''

    for key in allKeys: 
        glyph = chr(key)
        tmplt = Template(TPL_glyph)
        _glyph = tmplt.render(
                key=key,
                name=chr(key),
                )
        _output += _glyph

    f = open('glyphs.yaml', "w")
    f.write(_output)
    f.close()

def getPath(file):
    with open(file, 'rt') as f:
        tree = ElementTree.parse(f)
    root = tree.getroot()
    for path in root.iter():
        path_d = path.attrib.get('d')
        if path_d:
            result = path_d
    return result

def dotBuilder(path):
    dots = []

    d_dot = "m 1.12,0.15 0.27,0.26 c 0.2,0.2 0.2,0.51 0,0.71 L 1.13,1.39 c -0.2,0.2 -0.51,0.2 -0.71,0 L 0.15,1.12 c -0.2,-0.2 -0.2,-0.51 0,-0.71 L 0.41,0.15 c 0.2,-0.2 0.51,-0.2 0.71,0 z"

    p_dot = parse_path(d_dot)
    p_dot = p_dot.reversed()
    p_dot = p_dot.scaled(_INFO['handles-width'])
    ppbx = p_dot.bbox()
    ppw = ppbx[1] - ppbx[0]
    pph = ppbx[3] - ppbx[2]

    for p in path:
        vd = 1.5
        px = p.start.real - ppw/2
        py = p.start.imag - pph/2 
        tr_comp = complex(px, py)
        _dot = p_dot.translated(tr_comp)
        d = _dot.d()
        dot = '<path class="dot" d="'+d+'" stroke-linejoin="round"/>'
        dots.append(dot)
    px = p.end.real - ppw/2
    py = p.end.imag - pph/2 
    tr_comp = complex(px, py)
    _dot = p_dot.translated(tr_comp)
    d = _dot.d()
    dot = '<path class="dot" d="'+d+'" stroke-linejoin="round"/>'
    dots.append(dot)
    return dots

def buildSVG(key, displayGrid=True):
    g = _YAML_SETTINGS['glyphs'][key]
    _paths = []
    _dots = []

    if g['modules'] != None:
        for mod in g['modules']:
            k, v = list(mod.items())[0]
            try: 
                pth = getPath(_MODULES_DIR+k+'.svg')
                pth = parse_path(pth)
                dotBuilder(pth)
                xmin, xmax, ymin, ymax = pth.bbox()
                pth_height = ymax - ymin

                tr_x = v[0] 
                tr_y = v[1] + (_INFO['height'] - _INFO['baseline'] - pth_height)
                try: 
                    tr_x = tr_x + _INFO['translate-x']
                except:
                    print('no translate-x')
                tr_comp = complex(tr_x, tr_y)
            
                pp = pth.translated(tr_comp)

                if len(v) > 2:
                    trans_key, trans_value = v[2].split('=')
                    if trans_key == 'rotation':
                        ppbx = pp.bbox()
                        ppw = ppbx[1] - ppbx[0]
                        pph = ppbx[3] - ppbx[2]
                        ori_x = ppbx[1] - (ppw/2)
                        ori_y = ppbx[3] - (pph/2)
                        ori = complex(ori_x, ori_y)
                        pp = pp.reversed()
                        pp = pp.rotated(int(trans_value), origin=ori)

                dots = dotBuilder(pp)
                print('KEY ------>', key)
                _dots.append(dots)

                _paths.append(pp.d())
            except:
                print('module missing')

        grid_lines = []

        if displayGrid == True:
            height = _YAML_SETTINGS['info']['height'] 
            width = _YAML_SETTINGS['info']['width']
            baseline = height - _YAML_SETTINGS['info']['baseline']
            x_height = baseline - _YAML_SETTINGS['info']['x_height']
            ascendant = baseline - _YAML_SETTINGS['info']['ascendant']
            descendant = baseline - _YAML_SETTINGS['info']['descendant']

            line_struc = [baseline, x_height, ascendant, descendant]

            for ri in range(int(height)):
                line= '<line x1="0" y1="'+str(ri)+'" x2="'+str(width)+'" y2="'+str(ri)+'" stroke="blue" stroke-width=".01px" class="grid-line grid-line-y" />'
                grid_lines.append(line)

            for ri in range(int(width)):
                line= '<line x1="'+str(ri)+'" y1="0" x2="'+str(ri)+'" y2="'+str(height)+'" stroke="blue" stroke-width=".01px" class="grid-line grid-line-y" />'
                grid_lines.append(line)
            
            for ri in line_struc:
                line= '<line x1="0" y1="'+str(ri)+'" x2="'+str(width)+'" y2="'+str(ri)+'" stroke="red" stroke-width=".02px" class="grid-line grid-line-struc" />'
                grid_lines.append(line)


        TPL = '''<?xml version="1.0"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="350" height="1000" viewBox="0 0 {{width}} {{height}}" >
            {% for dot in dots: %}
                {{dot}}
            {% endfor %}
            {% for d in paths: %}
            <path d="{{d}}" fill="none"
            stroke="{{stroke}}"
            stroke-width="{{strokeWidth}}"
            stroke-linecap="{{strokeLinecap}}"
            stroke-linejoin="round"
            />
            {% endfor %}
            {% for l in grid_lines: %}
                {{l}}
            {% endfor %}
        </svg>'''

        tmplt = Template(TPL)
        _svg = tmplt.render(
                paths=_paths,
                dots=_dots,
                width=_INFO['width'],
                height=_INFO['height'],
                stroke=_INFO['stroke'],
                strokeWidth=_INFO['stroke-width'],
                strokeLinecap=_INFO['stroke-linecap'],
                grid_lines=grid_lines
                )

        return _svg


_YAML_SETTINGS = readYaml('sources/'+_YAML_FILE) 
_INFO = _YAML_SETTINGS['info']
_MODULES_DIR = 'modules_'+str(_VERSION)+'/'


## WEB !!!! 
app = Flask(__name__, static_url_path='/static')

@app.route('/')
def index():
    return render_template('index.html', test="yes")



@app.route('/flask_buildSVG', methods = ['POST'])
def flask_buildSVG():
    global _YAML_SETTINGS
    _YAML_SETTINGS = readYaml('sources/'+_YAML_FILE) 
    if request.method == 'POST':
        keys = request.get_json()
        fetch_reponse = []
        if keys[0] == '__ALL':
            keys = _YAML_SETTINGS['glyphs']

        for key in  keys:
            r = buildSVG(int(key))
            fetch_reponse.append({'key': int(key), 'svg': r})

    print(type(fetch_reponse))
     
    return fetch_reponse

@app.route('/flask_buildFont')
def flask_buildFont():
    global _YAML_SETTINGS
    _YAML_SETTINGS = readYaml('sources/'+_YAML_FILE) 
    keys = _YAML_SETTINGS['glyphs']
    reponse = []
    directory = 'svgs/'+_INFO['font-name']

    if not os.path.exists(directory):
        os.makedirs(directory)
        print("Directory created successfully!")

    for key in  keys:
        if _YAML_SETTINGS['glyphs'][key]['modules'] != None:
            _svg = buildSVG(key, displayGrid=False)
            f = open(directory+'/'+str(key)+".svg", "w")
            f.write(_svg)
            f.close()
    fg = font_generator.font_generator()
    fg.generate(_INFO)

    return reponse 

@app.route('/flask_changeversion/<version>')
def flask_changeversion(version):
    global _VERSION, _YAML_FILE, _MODULES_DIR, _YAML_SETTINGS, _INFO
    _VERSION = int(version) 
    _YAML_FILE = 'letters-build-'+str(_VERSION)+'.yaml'
    _YAML_SETTINGS = readYaml('sources/'+_YAML_FILE) 
    _INFO = _YAML_SETTINGS['info']
    _MODULES_DIR = 'modules_'+str(_VERSION)+'/'
    return str(_VERSION) 

if __name__ == '__main__':
    app.run(debug=False, port=4000)




