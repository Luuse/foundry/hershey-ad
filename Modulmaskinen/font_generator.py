import glob
try:
    import fontforge 
except:
    print('fontforge module is missing')

class font_generator():

    def __init__(self):
        self.svg_path = 'svgs/'
        self.font_path = 'static/export-fonts/'

    def generate(self, _INFO):
        print(_INFO['font-name'])
        font = fontforge.font()
        font.encoding = "UnicodeBmp"
        SVG_DIR = glob.glob(self.svg_path+'/'+_INFO['font-name']+'/*.svg')
        for g in SVG_DIR:
            gkey = g.split("/")[-1].replace(".svg", "")
            print(gkey)
            if gkey.isdigit() == True:
                letter_char = font.createChar(int(gkey))
                letter_char.importOutlines(g)
                letter_char.width = _INFO['fontforge-width'] 
        font.selection.all()
        font.correctDirection()
        font.fontname = _INFO['font-name']
        font.familyname = _INFO['font-family']
        font.fullname = _INFO['font-name-humans']
        font.copyright = _INFO['copyright']
        font.generate(self.font_path+'/'+_INFO['font-name']+'.otf')
        font.generate(self.font_path+'/'+_INFO['font-name']+'.ttf')


        return 'yes'

