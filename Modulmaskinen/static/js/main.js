
var _TESTING


async function ping(url) {
	let response = await fetch(url)
  return response.text()
}

function changeVersion(version) {
	ping('/flask_changeversion/'+version).then((data) => {
		console.log('sssss', data)
		input_load(_INPUT.value)
	})
}


async function postData(url, data) {
	const response = await fetch(url, {
    method: "POST", 
	// 	mode: "cors",
    // cache: "no-cache",
    // credentials: "same-origin", 
		headers: {
			"Content-Type": "application/json"
    },
		body: JSON.stringify(data),
	});

	console.log(response)

  return response.json()
}

function input_load(text){
	var keys = []
	console.log(text)
	if (text == '__ALL'){
		keys = ['__ALL']
	} else {
		var letter_list = text.split('')
		letter_list.forEach(function(item, i){ keys.push(item.charCodeAt(0)) })
	}

	_TESTING.innerHTML = ''
	postData('/flask_buildSVG', keys).then((data) => {
		console.log(data) 
		data.forEach(function(item, i){
			if (item['svg'] != null) {
			 _TESTING.innerHTML += item['svg']
			}

		})
		console.log(data) 
	});

}

window.addEventListener('DOMContentLoaded', function(){
	_TESTING = document.getElementById("testing")
	_INPUT = document.querySelector("#input")
	_SCALE = document.querySelector("#scale")
	input_load(_INPUT.value)

	_INPUT.addEventListener('change', function(){input_load(this.value)})

	_SCALE.addEventListener('change', function(){
		_TESTING.style.transform = 'scale('+this.value+')'
	})

})

