# Chronologie des événements

1967: Allen Vincent Hershey writes "Calligraphy for computers", published by U.S. Naval Weapons Laboratory {https://archive.org/details/hershey-calligraphy_for_computers/page/n47/mode/2up}

1976: A contribution to computer typesetting techniques : Tables of coordinates for Hershey's repertory of occidental type fonts and graphic symbols is published by The National Bureau of Standards{https://archive.org/details/contributiontoco424wolc}

2011: Windell Oskay releases Hershey Text, an Inkscape extension for rendering text with engraving fonts. {https://www.evilmadscientist.com/2011/hershey-text-an-inkscape-extension-for-engraving-fonts/}

2014: Techninja publishes Hersheytextjs, A port of the EMSL Hershey engraving font data from the Hershey Text Inkscape Plugin to JSON, capable of being rendered quickly via JavaScript & SVG. {http://techninja.github.io/hersheytextjs/}

2017: Luuse releases Hershey-Noailles from a webtool that generates fonts file from Techninja's Hersheytextjs datas. The 85 fonts generated are first used for Villa Noailles art center based in France. {https://gitlab.com/Luuse/Villa-Noailles/font-hershey-noailles/}

2024: AM-Stockholm Studio asked Luuse to recalibrate the typeface Futural-mono-14-2.5-handles on a grid system and to produce two other versions with a progressive reduction of the same grid. Three new fonts were created for the new visual identity of ArkDes Museum in Stockholm (Sweden).

# Mechanism & processus

Arkdes-Hershey had to be recalibrated to fit on three specific grids, from 40 unit height to 17. according to the visual identity developped by AM-Stockholm, meaning each vector points had to match on a intersection point of the grid.
[a-grid-neg.png]

In order to make the font more systematic and to avoid extra work, we decided to cut out the letters on a set of modules. For example, we redesigned a stem on the grid and used it for letters b, d, h, k, l, p, q... or a shoulder for the h, n, m. Thanks to this method, we designed 87 modules used for a set of 171 letters multiplied by 3 for a total of 513 glyphs.
[images/modules-sheet-neg.png]

Then, we developped a small program called Modulmaskinen written in Python allowing us to build the letters using yaml files. Each letter was build by calling the corresponding modules and by declaring its position. For example, the letter "a" looks like this:
  97:
    name: 'a'
    modules:
      - panse: [2, 0, 'rotation=0']
      - fut-x: [6, 0]
[images/letter-build-40.yaml.png]

+ interface
- font generation
- enjeux de la réduction de forme
- usage

# Formules

For version 40, the formula is as follows: 
body-size = (new pitch * 1000) / 8.819
---
For version 26, the formula is as follows: 
body-size = (new pitch * 1000) / 13.582
---
For version 17, the formula to be applied is as follows: 
body-size = (new pitch * 1000) / 20.763
---

Let's take the 40 version as an example:
The hershey-ad-40 font has a cast of 350pt for a body of 1000pt, which gives a grid of 350/14 units or 25pt (or 8.819mm per step).

To obtain the correct size in points for a 5mm grid, we'll need to do the following cross product:
(5mm multiplied by 1000 points) divided by 8.819 (new original size pitch), which gives us a body-size of 566.96pt.
So the correct body size for a 5mm grid using Hershey-Ad-40 font is (5*1000)/8,819 = 566,96 pt

Hershey-AD-40 (height=40 : width=14)
Hershey-AD-26 (height=26 : width=10)
Hershey-AD-17 (height=27 : width=7)

la fonte hershey-ad-40 a une chasse de 350pt pour un corps de 1000pt, ce qui donne une grille de 350/14 unités soit 25pt ou 8,819mm par pas. Pour obtenir le bon corps en points pour une grille de 5mm, on va devoir faire faire le produit en croix suivant: (51000)/8,819 (nouveau pas corps d'origine) / pas d'origine), ce qui nous donne un corps à 566,96pt.

Pour la version 40, la formule à appliquer est la suivante: corps = (nouveau pas * 1000) / 8,819
Pour la version 26, la formule à appliquer est la suivante: corps = (nouveau pas * 1000) / 13,582
Pour la version 17, la formule à appliquer est la suivante: corps = (nouveau pas * 1000) / 20,76